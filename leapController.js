// Store frame for motion and gesture functions
// Need it to get to the pointable when I do the circle gesture
var previousFrame = null;

// Setup Leap loop with frame callback function
var controllerOptions = {
 enableGestures: true,
 background: true,
 loopWhileDisconnected: false
};

// Leap default values
var leap_width = 300;
var leap_low = 100;
var leap_high = 300;
var leap_near = 0;
var leap_far = -100;
var leap_depth = leap_near - leap_far;
var leap_height = leap_high - leap_low;

// Settings
var fps = 24;
var changeColors = true;
var primary = [0x20, 0xEA, 0x10];
var secondary = [0xCD, 0x45, 0x1E];
var pointerColors = {'scroll': "#210EE4", 'red': "#EA1212"};
var pointerLabel = "Leap";
//var running

// Sage dimensions (gets set in setupDimensions())
var width = 1920;
var height = 1080;

// Pointer state
var application_interaction = true;
var leftClicked = false;
var rightClicked = false;
var position = {pointerX:width/2, pointerY:height/2};
var pointerColor = 'none';
var color = [0,0,0];
var pointerPrecision = 0;

// Miscellaneous
var lastLeapPos = [0, 0, 0];
var lastFrame = 0;

// SETUP
var setup = false;

// Main loop for leap
Leap.loop(controllerOptions, function(frame) {
  if (!setupEnvironment ())
   return;
  if (frame.timestamp < lastFrame + 1e6 / fps) // timestamp is in microseconds
  return;
  lastFrame = frame.timestamp;

  if (frame.hands.length == 1) {
    let hand = frame.hands[0];
    let thumb = hand.thumb;
    let index = hand.indexFinger;
    let middle = hand.middleFinger;
    let ring = hand.ringFinger;
    let pinky = hand.pinky; 

    if (index.extended) {
      if (middle.extended) {
	if (ring.extended && pinky.extended)
	  return;
	changePointerColor('scroll');
	if (!thumb.extended)
	  scrollPointer(index.tipPosition);
      } else {
	movePointer(index.tipPosition);
	mouseEvents(thumb, pinky);
      }
    }
  } else if (frame.hands.length > 1) {
    printWarning('Please only use one hand.');
    changePointerColor('red');
  }
  previousFrame = frame;
})
// Leap events
.use('handEntry')
.on('handFound', function (hand) {
  if (!setupEnvironment ())
   return;
  console.log ('Hand found');
  wsio.emit('startSagePointer', {label: pointerLabel, color: getColor(primary)});
  position.pointerX = hand.indexFinger.tipPosition[0] * width / leap_width + width / 2;
  position.pointerY = height - (hand.indexFinger.tipPosition[1] - leap_low) * height / leap_height;
  checkPosition();
  lastLeapPos = hand.indexFinger.tipPosition;
  wsio.emit('pointerPosition', position);
  console.log(position);
  pointerColor = 'scale';
  })
.on('handLost', function (hand) {
  if (!setupEnvironment ())
   return;
  console.log ('Hand lost');
  wsio.emit('stopSagePointer');
  pointerColor = 'none';
  clearCanvas();
  })
.on('gesture', function (gesture) {
  if (!setupEnvironment ())
   return;
  console.log('Gesture: ' + gesture.type);
  if (gesture.type == "circle")
  changePointerInteraction(gesture);
  })

function mouseEvents (thumb, pinky) {
  if (thumb && !thumb.extended) {
    if (leftClicked == false) {
      wsio.emit('pointerPress', { button: "left" });
      leftClicked = true;
      console.log("Pointer press left");
    }
  }
  else {
    if (leftClicked == true) {
      wsio.emit('pointerRelease', { button: "left" });
      leftClicked = false;
      console.log("Pointer release left");
    }
  }

  if (pinky && pinky.extended) {
    if (rightClicked == false) {
      wsio.emit('pointerPress', { button: "right" });
      rightClicked = true;
      console.log("Pointer press right");
    }
  }
  else {
    if (rightClicked == true) {
      wsio.emit('pointerRelease', { button: "right" });
      rightClicked = false;
      console.log("Pointer release right");
    }
  }
}

function changePointerInteraction (circle) {
  if (leftClicked || !previousFrame)
    return;
  let pointableID = circle.pointableIds[0];
  let pointable = previousFrame.pointable(pointableID);
  if (circle.state != 'stop' || circle.radius < 20 || pointable.type != 1)
    return;
  let clockwise = false;
  let direction = pointable.direction;
  if (!direction || !circle.normal) {
    return;
  }
  let dotProduct = Leap.vec3.dot(direction, circle.normal);
  if (dotProduct > 0)
    clockwise = true;
  if ((application_interaction && !clockwise)
   || (!application_interaction && clockwise)) {

    if (application_interaction)
      console.log("Pointer interaction changed to window mode");
    else
      console.log("Pointer interaction changed to application mode");
    application_interaction = !application_interaction;
    wsio.emit('keyDown', {code:16});
    wsio.emit('keyPress', {code:9});
    wsio.emit('keyUp', {code:16});
    // Restart poitner so it remembers interaction type when we change color
    if (changeColors) {
      wsio.emit('stopSagePointer');
      wsio.emit('startSagePointer', {label:pointerLabel, color: getColor(color)});
    }
  }
}

function movePointer(vector) {
  let precision = (vector[2] - leap_far) / leap_depth;
  if (precision < 0)
    precision = 0;
  else if (precision > 1)
    precision = 1;

  let delta = [vector[0] - lastLeapPos[0], vector[1] - lastLeapPos[1]];
  position.pointerX = position.pointerX + delta[0] * width / leap_width * precision;
  position.pointerY = position.pointerY - delta[1] * height / leap_height * precision;
  checkPosition();
  if (pointerColor != 'scale' || Math.abs(pointerPrecision - precision) > 0.1) {
    changePointerColor ('scale', precision);
    pointerPrecision = precision;
  }
  lastLeapPos = vector;
  wsio.emit('pointerPosition', position);
  drawPosition();
}

function checkPosition () {
  if (position.pointerX < 0)
    position.pointerX = 0;
  else if (position.pointerX > width)
    position.pointerX = width;
  if (position.pointerY < 0)
    position.pointerY = 0;
  else if (position.pointerY > height)
    position.pointerY = height;
}

function scrollPointer(vector) {
  let middle = (leap_high + leap_low) / 2;
  let delta = vector[1] - middle;
  // raw data are too coarse. Tried to make it slower by dividing by constant, but it's too fast when you try to scroll slowly and too slow when you want to scroll fast. Found exponential function to be pretty ok for this. Sign and abs are to make it good/fast both ways
  wsio.emit('pointerScroll', {wheelDelta:(Math.sign(delta)*Math.pow(4, Math.abs(delta / 100)))});
  console.log('Scroling ' + delta);
}
  
function changePointerColor (color, precision = 0) {
  if (!changeColors)
    return;
  if (color == 'scale') {
    changePointerColorWithPrecision(precision);
    return;
  }
  if (pointerColor == color)
    return;
  wsio.emit('stopSagePointer');
  wsio.emit('startSagePointer', {label:pointerLabel, color:pointerColors[color]});
  wsio.emit('pointerPosition', position); // to get active window
  pointerColor = color;
  console.log("Changed pointer to " + color);
}

function changePointerColorWithPrecision (precision) {
  pointerColor = 'scale';
  //wsio.emit('stopSagePointer');
  let complement = 1 - precision;
  color = [ Math.round(primary[0] * precision + secondary[0] * complement),
	    Math.round(primary[1] * precision + secondary[1] * complement),
	    Math.round(primary[2] * precision + secondary[2] * complement) ];
  wsio.emit('startSagePointer', {label:pointerLabel, color: getColor(color)});
}

function getColor (color) {
	return '#' + hex(color[0])
		   + hex(color[1])
		   + hex(color[2]);
}

function setColor (color, value) {
	color[0] = parseInt(value.substr(1, 2), 16);
	color[1] = parseInt(value.substr(3, 2), 16);
	color[2] = parseInt(value.substr(5, 2), 16);
}

function hex (num) {
	return num.toString(16).padStart(2, '0');
}
//================================================================================//
// Setup functions
//================================================================================//
function setupEnvironment () { //FIXME TODO FIXME
  if (setup)
   return true;
  if (!wsio || !displayUI || !interactor)
   return false;
  interactor.pointerKeyDown = pointerKeyDownMethod;
  setupKeyboard();
  setupDimensions();
  setup = true;
  return true;
}

// setup keyboard listeners and wall dimenstions

function setupKeyboard () {
  document.addEventListener('keydown', keydown, false);
  document.addEventListener('keyup', keyup, false);
  document.addEventListener('keypress', keypress, false);
}

function stopKeyboard () {
  document.removeEventListener('keydown', keydown, false);
  document.removeEventListener('keyup', keyup, false);
  document.removeEventListener('keypress', keypress, false);
}

function keydown (event) {
	wsio.emit('keyDown', {code: parseInt(event.keyCode, 10)});
}
function keyup (event) {
	wsio.emit('keyUp', {code: parseInt(event.keyCode, 10)});
}
function keypress (event) {
	wsio.emit('keyPress', {code: parseInt(event.keyCode, 10)});
}

function setupDimensions () {
  width = displayUI.config.totalWidth;
  height = displayUI.config.totalHeight;
}
//================================================================================//
// Functions for visualization of actual pointer (black) and leap position (red) on canvas
//================================================================================//
function drawPosition () {
  // Canvas
  let sage2UI = document.getElementById('sage2UICanvas');
  let ctx = sage2UI.getContext('2d');

  clearCanvas();

  let x = position.pointerX * sage2UI.width / width;
  let y = position.pointerY * sage2UI.height / height;
  ctx.fillStyle = '#121212';
  ctx.fillRect(x, y, 5, 5);

  let realX = lastLeapPos[0] * sage2UI.width / leap_width + sage2UI.width / 2;
  let realY = sage2UI.height - (lastLeapPos[1] - leap_low) * sage2UI.height / leap_height;
  ctx.fillStyle = '#EA1212';
  ctx.fillRect(realX, realY, 5, 5);
}

// If there is a problem use displayUI.draw(); (this function is halfly copied that one)
function clearCanvas () {
  let sage2UI = document.getElementById('sage2UICanvas');
  let ctx = sage2UI.getContext('2d');

  ctx.clearRect(0, 0, sage2UI.width, sage2UI.height);

  // tiled display layout
  let i;
  ctx.lineWidth = 2;
  ctx.strokeStyle = "rgba(86, 86, 86, 1.0)";
  let stepX = sage2UI.width / displayUI.config.layout.columns;
  let stepY = sage2UI.height / displayUI.config.layout.rows;
  ctx.beginPath();
  for (i = 1; i < displayUI.config.layout.columns; i++) {
    ctx.moveTo(i * stepX, 0);
    ctx.lineTo(i * stepX, sage2UI.height);
  }
  for (i = 1; i < displayUI.config.layout.rows; i++) {
    ctx.moveTo(0, i * stepY);
    ctx.lineTo(sage2UI.width, i * stepY);
  }
  ctx.closePath();
  ctx.stroke();
}

function printWarning (string) {
  let canvas = document.getElementById('sage2UICanvas');
  let ctx = canvas.getContext('2d');
  ctx.font = "30px Arial";
  ctx.fillStyle = "#EA1212";
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  ctx.fillText(string, canvas.width / 4, canvas.height / 4);
}
//================================================================================//
// Leap settings UI
//================================================================================//
function showLeapSettings () {
  stopKeyboard();
  showDialog('leapPointerDialog');
  document.getElementById('leapLabel').value = pointerLabel;
  document.getElementById('changePointerColor').checked = changeColors;
  document.getElementById('primaryColor').value = getColor(primary);
  document.getElementById('secondaryColor').value = getColor(secondary);
  document.getElementById('scrollColor').value = pointerColors['scroll'];
  document.getElementById('leapFps').value = fps;
  document.getElementById('leapWidth').value = leap_width;
  document.getElementById('leapLow').value = leap_low;
  document.getElementById('leapHigh').value = leap_high;
  document.getElementById('leapNear').value = leap_near;
  document.getElementById('leapFar').value = leap_far;

  disableColors();
}

function hideLeapSettings () {
  setupKeyboard();
  hideDialog('leapPointerDialog');
}

// TODO input validation
function updateLeapSettings () {
	pointerLabel = document.getElementById('leapLabel').value;
	changeColors = document.getElementById('changePointerColor').checked;
	setColor(primary, document.getElementById('primaryColor').value);
	if (changeColors) {
		setColor(secondary, document.getElementById('secondaryColor').value);
		pointerColors['scroll'] = document.getElementById('scrollColor').value;
	}
  fps = parseInt(document.getElementById('leapFps').value);
  leap_width = parseInt(document.getElementById('leapWidth').value);
  leap_low = parseInt(document.getElementById('leapLow').value);
  leap_high = parseInt(document.getElementById('leapHigh').value);
  leap_near = parseInt(document.getElementById('leapNear').value);
  leap_far = parseInt(document.getElementById('leapFar').value);
  leap_depth = leap_near - leap_far;
  leap_height = leap_high - leap_low;
  hideLeapSettings();
}

function disableColors () {
  if (document.getElementById('changePointerColor').checked) {
    document.getElementById('secondaryColor').disabled = '';
    document.getElementById('scrollColor').disabled = '';
    document.getElementById('secondaryColor').value = getColor(secondary);
    document.getElementById('scrollColor').value = pointerColors['scroll'];
  } else {
    document.getElementById('secondaryColor').disabled = 'disabled';
    document.getElementById('secondaryColor').value = '#FFFFFF';
    document.getElementById('scrollColor').disabled = 'disabled';
    document.getElementById('scrollColor').value = '#FFFFFF';
  }
}
//================================================================================//
// Helper debug functions
//================================================================================//
function startTest () {
  wsio.emit ('startSagePointer', {label:pointerLabel, color:"#CE2311"});
}
function stopTest () {
  wsio.emit ('stopSagePointer');
}
function positionTest (x, y) {
  wsio.emit('pointerPosition', { pointerX: x, pointerY: y });
}
function moveTest (x, y) {
  wsio.emit('pointerMove', { dx: x, dy: y });
}
function clickTest () {
	pressTest();
	releaseTest();
}
function pressTest () {
  wsio.emit('pointerPress', {button:"left"});
}
function realeaseTest () {
  wsio.emit('pointerRelease', {button:"left"});
}
//================================================================================//
// Redefinition of functions so that the page looks more sexy (added 8th button to menu)
//================================================================================//
/**
 * Resize menus
 *
 * @method resizeMenuUI
 * @param ratio {Number} scale factor
 */
resizeMenuUI = function (ratio) {
  if (!viewOnlyMode) {
    let menuContainer = document.getElementById('menuContainer');
    let menuUI        = document.getElementById('menuUI');

    // Extra scaling factor
    ratio = ratio || 1.0; 

    let menuScale = 1.0; 
    let freeWidth = window.innerWidth * ratio;
    if (freeWidth < 840) {
      // 9 buttons, 120 pixels per button
      // menuScale = freeWidth / 1080;
      // 10 buttons, 120 pixels per button
      // menuScale = freeWidth / 1200;
      // 8 buttons, 120 pixels per button
      menuScale = freeWidth / 960;
      // 7 buttons, 120 pixels per button
      //menuScale = freeWidth / 840; 
    }

    menuUI.style.webkitTransform = "scale(" + menuScale + ")"; 
    menuUI.style.mozTransform = "scale(" + menuScale + ")"; 
    menuUI.style.transform = "scale(" + menuScale + ")"; 
    menuContainer.style.height = parseInt(86 * menuScale, 10) + "px";

    // Center the menu bar
    let mw = menuUI.getBoundingClientRect().width;
    menuContainer.style.marginLeft = Math.round((window.innerWidth - mw) / 2) + "px";
  }
}
/**
 * Handler for key down
 *
 * @method pointerKeyDownMethod
 * @param event {Object} key event
 */
function pointerKeyDownMethod (event) {		    // For some reason does not get notified of esc
  // Get the code of the event
  var code = parseInt(event.keyCode, 10);
  // exit if 'esc' key
  if (code === 27) {
    interactor.stopSAGE2Pointer();
    if (event.preventDefault) {
      event.preventDefault();
    }
  } else {
    interactor.wsio.emit('keyDown', {code: code});
    if (code === 9) { // tab is a special case - must emulate keyPress event
      interactor.wsio.emit('keyPress', {code: code, character: String.fromCharCode(code)});
    }
    // if a special key - prevent default (otherwise let continue to keyPress)
    if (code === 8 || code === 9 || (code >= 16 && code <= 46 && code !== 32) ||
	(code >= 91 && code <= 93) || (code >= 112 && code <= 145)) {
      if (event.preventDefault) {
	event.preventDefault();
      }
    }
  }
};
